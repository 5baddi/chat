import { L10n, NodeBlock } from "tripetto-runner-foundation";
import { ReactNode } from "react";
import { FocusEvent } from "react";
import { IRuntimeStyles } from "@hooks/styles";
import { IRunnerAttachments, TRunnerViews } from "tripetto-runner-react-hook";

export interface IChatRenderProps {
    readonly id: string;
    readonly l10n: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly view: TRunnerViews;
    readonly name: JSX.Element | undefined;
    readonly description: JSX.Element | undefined;
    readonly explanation: JSX.Element | undefined;
    readonly label: JSX.Element | undefined;
    readonly placeholder: string;
    readonly submitLabel: string;
    readonly isFailed: boolean;
    readonly ariaDescribedBy: string | undefined;
    readonly ariaDescription: JSX.Element | undefined;
    readonly focus: <T>(e: FocusEvent) => T;
    readonly blur: <T>(e: FocusEvent) => T;
    readonly autoFocus: (element: HTMLElement | null) => void;
    readonly attachments: IRunnerAttachments | undefined;
    readonly markdownifyToJSX: (md: string, lineBreaks?: boolean) => JSX.Element;
    readonly markdownifyToURL: (md: string) => string;
    readonly markdownifyToImage: (md: string) => string;
    readonly markdownifyToString: (md: string) => string;
}

export interface IChatRendering extends NodeBlock {
    readonly required?: boolean;
    readonly question?: (props: IChatRenderProps) => ReactNode;
    readonly answer?: (props: IChatRenderProps) => ReactNode;
    readonly input?: (props: IChatRenderProps, done?: () => void, cancel?: () => void) => ReactNode;
}
