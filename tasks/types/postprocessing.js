const fs = require("fs");
const path = "./runner/types/index.d.ts";
const banner = require("../banner/banner.js");
const declaration = fs.readFileSync(path, "utf8").split("\n");
let errors = 12;

for (let row = 0; row < declaration.length; row++) {
    declaration[row] = declaration[row].replace("declare ", "");

    if (declaration[row].indexOf("private ") !== -1) {
        if (row > 0 && declaration[row - 1].indexOf("*/") !== -1) {
            let comment = row - 1;

            while (comment > 0 && declaration[comment].indexOf("/*") === -1) {
                declaration[comment] = "";

                comment--;
            }

            declaration[comment] = "";
        }

        declaration[row] = "";
    }

    if (
        declaration[row].indexOf("export {};") !== -1 ||
        declaration[row].indexOf(`export { IChat } from "@interfaces/chat";`) !== -1 ||
        declaration[row].indexOf(`export { IChatProps, TChatDisplay, TChatPause } from "@interfaces/props";`) !== -1 ||
        declaration[row].indexOf(`export { IChatSnapshot } from "@interfaces/snapshot";`) !== -1 ||
        declaration[row].indexOf(`export { IChatRunner } from "@interfaces/runner";`) !== -1 ||
        declaration[row].indexOf(`export { IChatStyles } from "@interfaces/styles";`) !== -1 ||
        declaration[row].indexOf(`export { IChatController } from "@hooks/controller";`) !== -1 ||
        declaration[row].indexOf(`export { IChatRendering, IChatRenderProps } from "@interfaces/block";`) !== -1 ||
        declaration[row].indexOf(`export { namespace } from "@namespace";`) !== -1
    ) {
        declaration[row] = "";
        errors--;
    }

    if (
        declaration[row].indexOf(
            `import { IDefinition, ISnapshot, Instance, NodeBlock, TL10n, TStyles } from 'tripetto-runner-foundation';`
        ) !== -1
    ) {
        declaration[
            row
        ] = `import { IDefinition, ISnapshot, Instance, L10n, NodeBlock, TL10n, TStyles } from 'tripetto-runner-foundation';`;
        errors--;
    }

    if (
        declaration[row].indexOf(
            `import { IRunnerAttachments, IRunnerProps, TRunnerPreviewData, TRunnerViews } from 'tripetto-runner-react-hook';`
        ) !== -1
    ) {
        declaration[row] += `\nimport styled from "styled-components";\n`;
        errors--;
    }

    if (declaration[row].indexOf(`export { css, keyframes } from "styled-components";`) !== -1) {
        declaration[row] = `export { styled };\n${declaration[row]}`;
        errors--;
    }
}

if (errors !== 0) {
    throw new Error("Declaration generation failed!");
}

const header = [
    `declare module "tripetto-runner-chat" {`,
    `import * as TripettoChat from "tripetto-runner-chat/module";`,
    `export * from "tripetto-runner-chat/module";`,
    `export { TripettoChat };`,
    `export default TripettoChat;`,
    `}`,
];

fs.writeFileSync(
    path,
    `/*! ${banner} */\n\n` + `${header.join("\n")}\n\n` + `declare module "tripetto-runner-chat/module" {\n${declaration.join("\n")}\n}\n`,
    "utf8"
);
