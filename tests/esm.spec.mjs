import TripettoChatAsDefaultImport from "../runner/esm/index.mjs";
import { TripettoChat, ChatRunner } from "../runner/esm/index.mjs";
import "../builder/esm/index.mjs";

try {
    if (typeof TripettoChatAsDefaultImport.ChatRunner === "undefined") {
        throw new Error("ES6 module failed!");
    }

    if (typeof TripettoChat.ChatRunner === "undefined") {
        throw new Error("ES6 module failed!");
    }

    if (typeof ChatRunner === "undefined") {
        throw new Error("ES6 module failed!");
    }
} catch(e) {
    throw new Error("ES6 module failed!");
}
